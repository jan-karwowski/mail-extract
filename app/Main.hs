module Main where

import System.Environment (getArgs)
import Operations
import Data.List

main :: IO ()
main = getArgs >>= mapM (uncurry extractEmailsToDir) . uncons >>= maybe (putStrLn "bad arguments") (either putStrLn prettyPrintResult)
