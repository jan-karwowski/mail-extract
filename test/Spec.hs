import MU.PicklersTest
import Test.HUnit
import EmailTests

main :: IO ()
main = runTestTT (TestList [picklerTests, emailTests]) >>= print
