module MU.PicklersTest where

import Control.Arrow
import Control.Arrow.ListArrow
import Mu.Picklers
import Mu.Types
import Test.HUnit
import Text.XML.HXT.Arrow.Pickle
import Text.XML.HXT.Arrow.ReadDocument
import Text.XML.HXT.Core

xmlContents :: String
xmlContents =
  "<messages>\
  \ <message>\
  \ <from>someaddr</from>\
  \ <to>otheraddr</to>\
  \ <subject>subj1</subject>\
  \ <date>1618500553</date>\
  \ <size>9672</size>\
  \ <msgid>ASDF</msgid>\
  \ <path>/dev/null</path>\
  \ <maildir>/some/INBOX</maildir>\
  \ </message>\
  \ <message>\
  \ <from>xaddr</from>\
  \ <to>yaddr</to>\
  \ <subject>subj2</subject>\
  \ <date>1618553</date>\
  \ <size>972</size>\
  \ <msgid>QQQ</msgid>\
  \ <path>/dev/zero</path>\
  \ <maildir>/other/INBOX</maildir>\
  \ </message>\
  \ </messages>"

parsedXml :: [Either String Messages]
parsedXml = (runLA $ xreadDoc >>> removeAllWhiteSpace >>> arr (unpickleDoc' xpMessages)) xmlContents

parsedContents :: Messages
parsedContents =
  Messages
    [ MessageDescriptor "someaddr" "otheraddr" "subj1" 1618500553 9672 "ASDF" "/dev/null" "/some/INBOX",
      MessageDescriptor "xaddr" "yaddr" "subj2" 1618553 972 "QQQ" "/dev/zero" "/other/INBOX"
    ]

unpickleTest :: Test
unpickleTest =
  TestCase $
    assertEqual "Parsed document is equal the reference" parsedXml [Right parsedContents]

picklerTests :: Test
picklerTests = TestList [TestLabel "unpickleTest" unpickleTest]
