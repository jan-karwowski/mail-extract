module EmailTests where

import Test.HUnit
import Attachments

extrTest1 :: Test
extrTest1 = TestCase $ assertEqual "simple email address" "a" (extractEmailLogin "a@example.com")
extrTest2 :: Test
extrTest2 = TestCase $ assertEqual "address with name" "brutus" (extractEmailLogin "Brutus <brutus@rome.it>")

emailTests :: Test
emailTests  = TestList [TestLabel "t1" extrTest1, TestLabel "t2" extrTest2]
