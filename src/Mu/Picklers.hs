module Mu.Picklers (xpMessageDescriptor, xpMessages, parseMessages) where

import Control.Arrow
import Control.Arrow.ListArrow
import qualified Mu.Types as MT
import Text.XML.HXT.Arrow.Pickle
import Text.XML.HXT.Core

uncurry8 :: (a -> b -> c -> d -> e -> f -> g -> h -> x) -> (a, b, c, d, e, f, g, h) -> x
uncurry8 fun (a, b, c, d, e, f, g, h) = fun a b c d e f g h

xpMessageDescriptor :: PU MT.MessageDescriptor
xpMessageDescriptor =
  xpWrap
    (uncurry8 MT.MessageDescriptor, MT.descriptorToTuple)
    $ xp8Tuple
      (xpElem "from" xpText)
      (xpElem "to" xpText)
      (xpElem "subject" xpText)
      (xpElem "date" xpInt)
      (xpElem "size" xpInt)
      (xpElem "msgid" xpText)
      (xpElem "path" xpText)
      (xpElem "maildir" xpText)

xpMessages :: PU MT.Messages
xpMessages =
  xpElem "messages" $
    xpWrap (MT.Messages, MT.unMessages) $
      xpList1 $ xpElem "message" xpMessageDescriptor



parseMessages :: String -> Either [Char] MT.Messages
parseMessages xmlString = checkSingle $ (runLA $ xreadDoc >>> removeAllWhiteSpace >>> arr (unpickleDoc' xpMessages)) xmlString
  where
    checkSingle [] = Left "Empty list returned"
    checkSingle [x] = x
    checkSingle (_ : r@(_ : _)) = checkSingle r
