module Mu.Find where

import Data.List
import qualified Data.Map as M
import Mu.Picklers
import Mu.Types
import System.IO
import System.Process

muFind :: [[Char]] -> IO (Either [Char] Messages)
muFind args = do
  (_, Just hout, _, _) <- createProcess (proc "mu" ("find" : "--format" : "xml" : args)) {std_out = CreatePipe}
  parseMessages <$> hGetContents hout

latestMailForAuthor :: Messages -> M.Map String MessageDescriptor
latestMailForAuthor = foldl' combine M.empty . unMessages
  where
    combine mp msg = M.alter (altf msg) (from msg) mp
    altf x Nothing = Just x
    altf x (Just y) = Just $ if date x > date y then x else y

latestMessageFileForAuthor :: Messages -> M.Map String FilePath
latestMessageFileForAuthor = fmap path . latestMailForAuthor
