module Mu.Types(MessageDescriptor(..), Messages(..), descriptorToTuple) where

data MessageDescriptor = MessageDescriptor
  { from :: String,
    to :: String,
    subject :: String,
    date :: Int,
    size :: Int,
    msgid :: String,
    path :: FilePath,
    maildir :: String
  } deriving (Show, Eq)

descriptorToTuple :: MessageDescriptor -> (String, String, String, Int, Int, String, FilePath, String)
descriptorToTuple = (,,,,,,,) <$> from <*> to <*> subject <*> date <*> size <*> msgid <*> path <*> maildir

newtype Messages = Messages {unMessages :: [MessageDescriptor]} deriving (Show, Eq)

