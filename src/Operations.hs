module Operations where

import Attachments
import Mu.Find
import Control.Monad.Except
import qualified Data.Map as M
import Data.Monoid.Colorful


extractEmailsToDir :: FilePath  -> [String] -> IO (Either String [(String, ExtractionResult)])
extractEmailsToDir outDir query = runExceptT $ do
 res <- liftIO (muFind query) >>= liftEither
 let rr = latestMessageFileForAuthor res
 liftIO $ mapM (\(ad, em) -> (,)ad <$> extractArchiveFromMail outDir ad em) $ M.toList rr


prettyPrintResult :: [(String, ExtractionResult)] -> IO ()
prettyPrintResult res = do
  term <- getTerm
  printColoredS term $ mconcat $ map printSingleResult res
  where
    printSingleResult (email, ExtractionSuccess) = Fg Green $ Value $ email ++ "\n"
    printSingleResult (email, err) = Fg Red $ Value $ email ++ " " ++ show err ++ "\n"
