module Attachments where

import Control.Monad
import Control.Monad.Except
import Control.Monad.Trans.Resource
import Data.List
import Data.Maybe
import GHC.IO.Exception
import System.Directory
import System.FilePath
import System.IO.Temp
import System.Process
import Data.Either.Extra

data ExtractionResult = ExtractionSuccess | ExtractionFailure | NoArchiveAttachments | ManyArchiveAttachments | EmailFormatError deriving (Show)

archiveExtensions :: [String]
archiveExtensions = ["zip", "tar", "7z", "tar.gz", "tar.xz", "tar.bz2"]

matchExtention :: [String] -> FilePath -> Maybe (FilePath, String)
matchExtention exts path = (,) path <$> find (`isSuffixOf` path) exts

extractEmailLogin :: String -> String
extractEmailLogin = takeWhile (/= '@') . extractAngleBrackets

extractAngleBrackets :: String -> [Char]
extractAngleBrackets eml = if not (null (extr eml)) then extr eml else eml
  where
    extr = takeWhile (/= '>') . drop 1 . dropWhile (/= '<')

splitOn :: (Eq a) => a -> [a] -> [[a]]
splitOn el = uncurry (:) . foldr (\ch (curr, sp) -> if ch == el then ([], curr : sp) else (ch : curr, sp)) ([], [])

sortEmailFields :: String -> Maybe String
sortEmailFields = sortFields . splitOn '.'
  where
    sortFields [] = Just ""
    sortFields [x] = Just x
    sortFields [x, y] = Just $ y ++ "." ++ x
    sortFields [x, y, "stud"] = Just $ y ++ "." ++ x
    sortFields _ = Nothing

extractArchiveFromMail :: FilePath -> String -> FilePath -> IO ExtractionResult
extractArchiveFromMail outDir email msgFile = fmap (either id id) $
  runResourceT $
    runExceptT $ do
      (_, tempDir) <- createTempDirectory Nothing "mail-extract-temp"
      code <- liftIO $ rawSystem "ripmime" ["ripmime", "-i", msgFile, "-d", tempDir]
      when (code /= ExitSuccess) $ throwError ExtractionFailure
      files <- fmap (mapMaybe $ matchExtention archiveExtensions) $ liftIO $ listDirectory tempDir
      (archFile, ext) <- case files of
        [] -> throwError NoArchiveAttachments
        [f] -> return f
        _ -> throwError ManyArchiveAttachments
      prefix <- liftEither $ maybeToEither EmailFormatError  (sortEmailFields (extractEmailLogin email)) 
      liftIO $ putStrLn $ "Renaming.. " ++ (tempDir </> archFile) ++ " " ++ (outDir </> prefix <.> ext)
      liftIO $ copyFile (tempDir </> archFile) (outDir </> prefix <.> ext)
      return ExtractionSuccess
